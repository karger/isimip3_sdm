#' A class to hold objects of classes glm, randomForest.formula, and gam.
#'
#' This class is designed to store objects of different classes such as glm, randomForest.formula, and gam.
#' It is useful for organizing and managing multiple models of different types.
#'
#' @name ModelCollection
#' @aliases ModelCollection-class
#' @rdname ModelCollection
#' @export
setClass("ModelCollection",
         slots = c(glm = "glm",
                   randomForest = "ANY",
                   gam = "ANY"),
         contains = "list")

#' Function to get a slot value from a ModelCollection object by name
#'
#' @param object An object of class ModelCollection.
#' @param slot_name The name of the slot to retrieve.
#' @return The value of the slot specified by slot_name.
#' @export
get_slot <- function(object, slot_name) {
  if (slot_name %in% slotNames("ModelCollection")) {
    return(slot(object, slot_name))
  } else {
    stop("Slot '", slot_name, "' does not exist in ModelCollection.")
  }
}

# Constructor function to create objects of class ModelCollection
createModelCollection <- function(glm_obj = NULL, randomForest_obj = NULL, gam_obj = NULL) {
  new("ModelCollection", glm = glm_obj, randomForest = randomForest_obj, gam = gam_obj)
}
