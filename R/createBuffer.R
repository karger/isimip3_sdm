  #' Create a Buffer Around Spatial Points
  #'
  #' This function creates a buffer of a specified distance around a given set of spatial points.
  #'
  #' @param points A `SpatVector` object representing the spatial points to buffer.
  #' @param buffer_distance_km A numeric value specifying the buffer distance in kilometers.
  #'
  #' @return A `SpatVector` object representing the buffered points.
  #'
  #' @examples
  #' # Load required package
  #' library(terra)
  #'
  #' # Example points data
  #' points <- vect(matrix(c(0, 0, 1, 1), ncol = 2), crs = "EPSG:4326")
  #'
  #' # Create a buffer of 1000 kilometers around the points
  #' buffered_points <- create_buffer(points, 1000)
  #'
  #' @export
createBuffer <- function(points, buffer_distance_km, ...) {
  # Convert the buffer distance from kilometers to meters
  buffer_distance_m <- buffer_distance_km * 1000

  # Create the buffer around the points
  buffered_points <- buffer(points, width = buffer_distance_m)

  return(buffered_points)
}