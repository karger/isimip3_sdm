#' Scale Parameters Function
#'
#' Scales the input vector \code{x} using the provided mean and standard deviation parameters.
#'
#' @param x A numeric vector to be scaled.
#' @param mean The mean value used for scaling.
#' @param sd The standard deviation value used for scaling.
#' @param ... Additional arguments (currently not used).
#' @return A scaled vector using the provided mean and standard deviation.
#' @examples
#' # Example usage:
#' scaled_values <- scaleParm(x = c(1, 2, 3), mean = 2, sd = 0.5)
#' print(scaled_values)
#' @export
scaleParm <- function(x, mean, sd, ...) {
  f <- (x - mean)/sd #scale(x) * sd + mean
  return(f)
}
