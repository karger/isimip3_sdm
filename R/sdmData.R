#' SdmData Class
#'
#' A class to hold spatial data for species distribution modeling.
#'
#' @slot dat_sp A SpatialPointsDataFrame object containing species occurrence data.
#' @slot tss_mat A numeric matrix containing rankings of environmental variables.
#' @slot selvars A character vector containing selected environmental variables.
#' @slot scale_params An object containing parameters for scaling the environmental variables.
#'
#' @name SdmData
#' @export
setClass("SdmData",
  slots = c(
    dat_sp = "SpatialPointsDataFrame",
    tss_mat = "ANY",
    selvars = "ANY",
    scale_params = "ANY"
  )
)

# Constructor function for the new class
SdmObjects <- function(dat_sp, tss_mat, selvars, scale_params) {
  new("SdmData", dat_sp = dat_sp, tss_mat = tss_mat, selvars = selvars, scale_params = scale_params)
}