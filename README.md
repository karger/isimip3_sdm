# ISIMIP3_SDM

## Introduction

An R package to calculate species distribution models for ISIMIP3 

## Installation

isimip3sdm is under development. Until release, you can only install it from 
gitlabext.wsl.ch. 

To install the package you will need to have the R package devtools installed
```
install.packages("devtools")
library(devtools)
install_git("https://gitlabext.wsl.ch/karger/isimip3_sdm.git")
```

## Usage
isimip3sdm provides serveral functions that allow calculating SDMs for a set of presence absence points and 
environmental data.

### Examples

### Modeling the distribution of a single species 



### install the package from source
```R
install.packages("~/PycharmProjects/isimip3_sdm",
repos = NULL,
type = "source")

library(isimip3sdm)
library(terra)
library(mgcv)
```

### set the input and output directories
```R
input <- "/storage/karger/isimip3b_bioclim/1981_2010/"

speciesname <- "Ischnocnema_penaxavantinho"
outputpath <- "/home/karger/scratch/"
inputpath <- "/home/karger/Documents/bio/Bio_Backup/ISIMIP/SDMs/Amphibian_Pseudoabsences/"
envroot <- "/storage/karger/isimip3b_bioclim/1981_2010/"
envname <- "obs_1981_2010"
```

# load the library
```R
library(isimip3sdm)
```

# load environmental data
```R
env <- rast(paste0(input,"bioclim.nc"))
env <- c(env[["bio1"]],env[["bio4"]],env[["bio12"]],env[["bio15"]])

bioclim_plus <- c(rast(paste0(input,"swb.nc")),
                  rast(paste0(input,"relative_humidity_stats.nc")),
                  rast(paste0(input,"fd.nc"))
)

env <- c(env, flip(bioclim_plus))

```

# load species data
```R
pa <- load(paste0(inputpath, speciesname, "_PA.Rdata"))
pa <- allData[1]$PA1
pa <- pa[,1:3]
coordinates(pa) <- ~x+y
```

# fit a set of SDMs. 

The function fitSingleSpecies selects a set of variables from the environmental data that are not higher correlated 
than 0.7 Pearson correlation, and a maximum of variables so that for each variable there are at least 10 data points.
Environmental variables are scaled and the scaling parameters are saved in the fitted_models object. 
It provides validation statistics for the full model and a x-fold cross validation that is run n-times. It saves
the spatial predictions to a netcdf4 file if an outputpath is given.

```R
mods <- fitSingleSpecies(pa, speciesname, inputpath, env, outputpath)
```

# predict SDMs

the fitted_models object can then be predicted spatially using another set of environmental data. The environmental data
is scaled depending on the scaling that has been used for the model fitting and variable selection. Units of the 
new environmental data and names must be the same. The output is saved to a Netcdf file. 

```R
model = 'mpi-esm1-2-hr'
ssp = 'ssp585'
time = '2071_2100'
envroot <- paste0(envroot = "/storage/karger/isimip3b_bioclim/",time,"/",model,"/",ssp,"/")
envname <- paste0(model,"_",ssp,"_",time)

env <- rast(paste0(envroot,"bioclim.nc"))
env <- c(env[["bio1"]],env[["bio4"]],env[["bio12"]],env[["bio15"]])

bioclim_plus <- c(rast(paste0(input,"swb.nc")),
                  rast(paste0(input,"relative_humidity_stats.nc")),
                  rast(paste0(input,"fd.nc")))

env <- c(env, flip(bioclim_plus))

predictSingleSpecies(pa, mods, env, envname, speciesname,
                     outputpath, sdm_models=c("glm", "gam","randomForest"))
```
