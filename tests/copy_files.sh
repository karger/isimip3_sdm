#!/bin/bash

# Directories containing the species information
pseudoabsences_dirs=(
    "/data/nfs4/nfs/karger/isimip3b_bioclim/occ/Amphibian_Pseudoabsences"
    "/data/nfs4/nfs/karger/isimip3b_bioclim/occ/Mammal_Pseudoabsences"
    "/data/nfs4/nfs/karger/isimip3b_bioclim/occ/Bird_Pseudoabsences"
    "/data/nfs4/nfs/karger/isimip3b_bioclim/occ/Reptile_Pseudoabsences"
)

pseudoabsences_dirs=(
    "/data/nfs4/nfs/karger/isimip3b_bioclim/occ/Mammal_Pseudoabsences"
)

# Directory containing the files to be sorted
source_dir="/data/nfs4/nfs/karger/mammals"

# Output base directory for sorted files (specify your desired output path)
output_base_dir="/data/nfs4/nfs/karger/sorted_files"

# Loop through each Pseudoabsences directory
for pseudo_dir in "${pseudoabsences_dirs[@]}"; do
    # Extract group name (Amphibian, Mammal, Bird, Reptile) from the directory name
    group=$(basename "$pseudo_dir" | cut -d'_' -f1)
    echo "Processing group: $group"

    # Find all species names in the Pseudoabsences directory
    species_files=("$pseudo_dir"/*.Rdata)

    # If no species files are found, skip to the next directory
    if [ ${#species_files[@]} -eq 0 ]; then
        echo "No species files found in $pseudo_dir. Skipping..."
        continue
    fi

    # Loop through each species file to extract the species name
    for species_file in "${species_files[@]}"; do
        # Extract species name from the file name (e.g., Cavia_tschudii from Cavia_tschudii_PA.Rdata)
        species_name=$(basename "$species_file" | cut -d'_' -f1,2)
        echo "Processing species: $species_name"

        # Find all matching files in the source directory for this species
        matching_files=()
        while IFS= read -r -d '' file; do
            matching_files+=("$file")
        done < <(find "$source_dir" -maxdepth 1 -type f -name "${species_name}_*.nc" -print0)

        # If no matching files are found, skip to the next species
        if [ ${#matching_files[@]} -eq 0 ]; then
            echo "No matching files found for species $species_name. Skipping..."
            continue
        fi

        # Loop through each matching file and sort into the correct directory
        for file in "${matching_files[@]}"; do
            # Extract details from the filename
            filename=$(basename "$file")

            # Check and debug unexpected filenames
            echo "Processing file: $filename"

            # Match filenames like Cavia_tschudii_randomForest_ukesm1-0-ll_ssp126_2041_2070.nc
            if [[ $filename =~ ^(${species_name})_([^_]+)_([^_]+)_([^_]+)_([^_]+)_(.*)\.nc$ ]]; then
                sdm="${BASH_REMATCH[2]}"
                gcm="${BASH_REMATCH[3]}"
                ssp="${BASH_REMATCH[4]}"
                time="${BASH_REMATCH[5]}"d

                # Create the directory path (without species name)
                output_dir="${output_base_dir}/${group}/${time}/${gcm}/${ssp}/${sdm}"
                mkdir -p "$output_dir"

                # Copy the file to the correct directory
                echo "Copying $file to $output_dir"
                cp "$file" "$output_dir"

            # Match filenames like Cavia_tschudii_SDMobject.rds
            elif [[ $filename =~ ^(${species_name})_SDMobject\.rds$ ]]; then
                # Place SDMobject files in a separate directory
                output_dir="${output_base_dir}/${group}/SDMobjects/${sdm}"
                mkdir -p "$output_dir"

                # Copy the file to the correct directory
                echo "Copying $file to $output_dir"
                cp "$file" "$output_dir"

            # Check for additional cases with filenames like Cavia_tschudii_randomForest_obs_1981_2010.nc
            elif [[ $filename =~ ^(${species_name})_([^_]+)_obs_([^_]+)_(.*)\.nc$ ]]; then
                sdm="${BASH_REMATCH[2]}"
                time="${BASH_REMATCH[3]}"

                # Create directory path for observed data
                output_dir="${output_base_dir}/${group}/${time}/obs/${sdm}"
                mkdir -p "$output_dir"

                # Copy the file to the correct directory
                echo "Copying $file to $output_dir"
                cp "$file" "$output_dir"

            else
                echo "File $file does not match expected pattern. Skipping..."
            fi
        done
    done
done

echo "Copying complete!"
