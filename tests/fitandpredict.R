#!/usr/bin/env R
#' @title Species Distribution Modeling and Future Projection Script for ISIMIP3b
#'
#' @description
#' This script is designed to fit Species Distribution Models (SDMs) for a specified species using current environmental
#' data and to project the models under future climate scenarios.
#' The script takes command-line arguments to specify the species identifier and the paths for input and output data.
#'
#' @usage
#' ./script_name.R <spec_id> <outputpath> <inputpath>
#'
#' @param spec_id Integer. The index of the species to model, corresponding to the order of species in the input directory.
#' @param outputpath Character. The path where the output NetCDF files and model results will be saved.
#' @param inputpath Character. The path where the input presence-absence data files are stored.
#'
#' @details
#' The script performs the following steps:
#' 1. Parses command-line arguments to determine which species to model and where to find and store data.
#' 2. Loads the necessary R libraries for species distribution modeling.
#' 3. Loads the presence-absence data for the specified species from the input path.
#' 4. Loads the current environmental data and fits the SDMs (GLM, GAM, and RandomForest) for the species.
#' 5. Projects the models under future climate scenarios using specified General Circulation Models (GCMs) and Shared Socioeconomic Pathways (SSPs).
#' 6. Saves the results as NetCDF files to the specified output path.
#'
#' @note
#' Ensure that the necessary R packages (`isimip3sdm`, `terra`, `mgcv`) are installed and accessible.
#' The script is intended to be run from the command line and is not interactive.
#'
#' @examples
#' # Example command to run the script
#' ./fitandpredict.R 1 "/home/${USER}/output/" "/home/${USER}/input/"
#'
#' @author
#' Your Name - dirk.karger@example.com

library(terra)
library(mgcv)
library(isimip3sdm)

args <- commandArgs(trailingOnly=TRUE)
args
contents <- strsplit(args, " ")

spec_id <- as.numeric(contents[[1]])
outputpath <- as.character(contents[[2]]) # "/storage/karger/isimip3b_sdms/"
inputpath <- as.character(contents[[3]]) # "/home/karger/sdm_input/Amphibian_Pseudoabsences/"

print_colored <- function(text, color_code) {
  cat(paste0(color_code, text, "\033[0m", "\n"))
}
print_colored("Loading input...", "\033[31m")

# list the species
file_names <- list.files(inputpath)
species_names <- gsub("_PA\\.Rdata$", "", file_names)

# load current environmental data for fitting
speciesname <- species_names[spec_id]
print_colored(paste0("Speciesname: ", speciesname), "\033[32m")

envroot <- "/storage/karger/isimip3b_bioclim/1981_2010/"
envname <- "obs_1981_2010"

env <- rast(paste0(envroot,"bioclim.nc"))
env <- c(env[["bio1"]],env[["bio4"]],env[["bio12"]],env[["bio15"]])
bioclim_plus <- c(rast(paste0(envroot,"swb.nc")),
                  rast(paste0(envroot,"relative_humidity_stats.nc"))
)
env <- c(env, flip(bioclim_plus))

#load species data
pa <- load(paste0(inputpath, speciesname, "_PA.Rdata"))
pa <- allData[1]$PA1
pa <- pa[,1:3]
coordinates(pa) <- ~x+y

# fit the models
print_colored("Fitting...", "\033[31m")
mods <- fitSingleSpecies(pa, speciesname, env, outputpath=outputpath, sdm_models=c("glm", "gam","randomForest"),
                             buffer_distance_km = 1000, k=5, nrepits = 10, thrs = 0.7)

# save the fitted models object to file
mods_name <- file.path(outputpath, paste0(speciesname, "_fittedSDMobject.rds"))
saveRDS(mods, mods_name)

# predict models for future conditions
models <- c('mpi-esm1-2-hr', 'mri-esm2-0', 'gfdl-esm4', 'ipsl-cm6a-lr', 'ukesm1-0-ll')
ssps <- c('ssp126', 'ssp370', 'ssp585')
times <- c('2041_2070', '2071_2100')

print_colored("Predicting...", "\033[31m")
for (model in unique(models)){
  for (ssp in unique(ssps)){
    for (time in unique(times)){
      print_colored(paste0("GCM:",model," SSP:",ssp," time:",time), "\033[34m")
      envroot <- paste0(envroot = "/storage/karger/isimip3b_bioclim/",time,"/",model,"/",ssp,"/")
      envname <- paste0(model,"_",ssp,"_",time)

      env <- rast(paste0(envroot,"bioclim.nc"))
      env <- c(env[["bio1"]],env[["bio4"]],env[["bio12"]],env[["bio15"]])

      bioclim_plus <- c(rast(paste0(envroot,"swb.nc")),
                        rast(paste0(envroot,"relative_humidity_stats.nc"))
      )

      env <- c(env, flip(bioclim_plus))

      predictSingleSpecies(pa, mods, env, envname, speciesname,
                                 outputpath, sdm_models=c("glm", "gam","randomForest"))
    }
  }
}

