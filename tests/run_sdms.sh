#!/bin/bash

#SBATCH --job-name=ISIMIP3SDM
#SBATCH -A node # Node Account
#SBATCH --mail-user=dirk.karger@wsl.ch
#SBATCH --mail-type=SUBMIT,END,FAIL
#SBATCH --output=/home/karger/logs/ISIMIP3SDM_%A_%a.out.out
#SBATCH --error=/home/karger/logs/ISIMIP3SDM_%A_%a.err
#SBATCH --time=48:00:00
#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=3G
#SBATCH --ntasks=1

SCRIPT=/home/karger/scripts/isimip3_sdm/tests/fitandpredict.R
output=/home/karger/scratch/${2}/${SLURM_ARRAY_TASK_ID}/

# create the temporary directory
if [ -d "$output" ];
then
  echo "exists"
else
	mkdir $output
fi

singularity exec -B /storage /home/karger/singularity/isimip3sdm.sif Rscript --vanilla ${SCRIPT} ${SLURM_ARRAY_TASK_ID} ${output} ${1}

cp ${output}* /storage/karger/mammals/

rm -r ${output}